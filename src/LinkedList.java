import java.util.Collection;

class LinkedList<E> {

    private static class Node<E> {
        E item;
        Node<E> next;
        Node<E> prev;

        Node(Node<E> prev, E item, Node<E> next) {
            this.prev = prev;
            this.item = item;
            this.next = next;
        }
    }

    private int size = 0;

    private Node<E> first;
    private Node<E> last;


    LinkedList() {

        first = last = null;
    }

    int getSize() {
        return size;
    }

    void add(int index, E element) {
        if (!checkPositionIndex(index)) {
            System.out.println("Input incorrect!");
            return;
        }
        if (index == size)
            addLast(element);
        else
            addBefore(element, node(index));
    }

    void addFirst(E e) {
        final Node<E> f = first;
        final Node<E> newNode = new Node<>(null, e, f);
        first = newNode;
        if (f == null)
            last = newNode;
        else
            f.prev = newNode;
        size++;
    }

    void addLast(E e) {
        final Node<E> l = last;
        final Node<E> newNode = new Node<>(l, e, null);
        last = newNode;
        if (l == null) {
            first = newNode;
        } else {
            l.next = newNode;
        }
        size++;

    }

    boolean remove(int index) {
        if (!checkPositionIndex(index)) return false;

        Node<E> removeItem = node(index);
        final Node<E> next = removeItem.next;
        final Node<E> prev = removeItem.prev;

        if (prev == null) {
            first = next;
        } else {
            prev.next = next;
            removeItem.prev = null;
        }

        if (next == null) {
            last = prev;
        } else {
            next.prev = prev;
            removeItem.next = null;
        }

        removeItem.item = null;
        size--;
        return true;
    }

    boolean remove(E item) {

        return remove(contains(item));
    }

    private void addBefore(E e, Node<E> position) {

        final Node<E> pred = position.prev;
        final Node<E> newNode = new Node<>(pred, e, position);
        position.prev = newNode;
        if (pred == null)
            first = newNode;
        else
            pred.next = newNode;
        size++;
    }

    int contains(Object o) {

        int index = 0;
        if (o == null) {
            for (Node<E> x = first; x != null; x = x.next) {
                if (x.item == null)
                    return index;
                index++;
            }
        } else {
            for (Node<E> x = first; x != null; x = x.next) {
                if (o.equals(x.item))
                    return index;
                index++;
            }
        }
        return -1;
    }

    void clear() {
        for (Node<E> x = first; x != null; ) {
            Node<E> nextNode = x.next;
            x.item = null;
            x.next = null;
            x.prev = null;
            size--;
            x = nextNode;
        }
        first = last = null;
    }

    public boolean addAll(int index, Collection<? extends E> c) {
        checkPositionIndex(index);

        Object[] a = c.toArray();
        int numNew = a.length;
        if (numNew == 0)
            return false;

        Node<E> predNode, postNode;
        if (index == size) {
            postNode = null;
            predNode = last;
        } else {
            postNode = node(index);
            predNode = postNode.prev;
        }

        for (Object o : a) {
            @SuppressWarnings("unchecked") E e = (E) o;
            Node<E> newNode = new Node<>(predNode, e, null);
            if (predNode == null)
                first = newNode;
            else
                predNode.next = newNode;
            predNode = newNode;
        }

        if (postNode == null)

        {
            last = predNode;
        } else

        {
            predNode.next = postNode;
            postNode.prev = predNode;
        }

        size += numNew;
        return true;
    }

    boolean isEmpty() {
        if (size == 0) {
            System.out.println("List is empty");
            return true;
        } else
            return false;


    }

    private boolean checkPositionIndex(int index) {
        return index >= 0 && index <= size;
    }

    private Node<E> node(int index) {
        // assert isElementIndex(index);
        if (index < (size >> 1)) {
            Node<E> x = first;
            for (int i = 0; i < index; i++)
                x = x.next;
            return x;
        } else {
            Node<E> x = last;
            for (int i = size - 1; i > index; i--)
                x = x.prev;
            return x;
        }
    }

    void print() {
        if (isEmpty()) {
            return;
        }
        Node<E> f = first;
        while (true) {
            System.out.print(f.item + " ");
            if (f.next == null) {
                System.out.println();
                return;
            }
            f = f.next;
        }

    }

}
